// A simple data API that will be used to get the data for our
// components. On a real website, a more robust data fetching
// solution would be more appropriate.
const PlayerAPI = {
    players: [
        { number: 1, name: "Indra Blocker", position: "G" },
        { number: 2, name: "Imran Defender", position: "D" },
        { number: 3, name: "Ari Sweeper", position: "D" },
        { number: 4, name: "Anggi Midfielder", position: "M" },
        { number: 5, name: "Surya Winger", position: "M" },
        { number: 6, name: "Atmadja Forward", position: "F" }
    ],
    all: function() {
        return this.players;
    },
    get: function(id) {
        const isPlayer = p => p.number === id;
        return this.players.find(isPlayer);
    }
};

export default PlayerAPI;
